using System;

namespace CodingExercises
{
    public static class ComparableExtensions
    {
        public static bool Lt<T>(this T a, T b) where T : IComparable {
            CheckTypes(a, b);
            return a.CompareTo(b) < 0;
        }

        public static bool Leq<T>(this T a, T b) where T : IComparable {
            CheckTypes(a, b);
            return a.CompareTo(b) <= 0;
        }

        public static bool Eq<T>(this T a, T b) where T : IComparable {
            CheckTypes(a, b);
            return a.CompareTo(b) == 0;
        }

        public static bool Geq<T>(this T a, T b) where T : IComparable {
            CheckTypes(a, b);
            return a.CompareTo(b) >= 0;
        }

        public static bool Gt<T>(this T a, T b) where T : IComparable {
            CheckTypes(a, b);
            return a.CompareTo(b) > 0;
        }

        private static void CheckTypes(object a, object b) {
            if (a.GetType() != b.GetType()) {
                throw new InvalidCastException("Objects have different types");
            }
        }
    }
}