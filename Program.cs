﻿using System;

namespace CodingExercises
{
    class Program
    {
        static void Main(string[] args)
        {
            var bubble = new BubbleSort();
            bubble.RunTests();

            Console.WriteLine("Done");
        }
    }
}
