# About

This repository is supposed to be a simple framework to make practicing algorithms easier (and also a chance for me to practice writing generic classes)

# Sorting Algorithms

There is an abstract `SortingAlgorithm` class in the folder of the same name.

When you want to implement your own sorting algorithm you should make a class that implements that class, where you will need to implement the `SortImpl` method.

Note that the elements in the array can be compared using the extension methods in `ComparableExtensions.cs`. This includes `.Lt()` for 'less than', `.Leq()` for 'less than or equal to' etc.

Note also that you should avoid using the inbuilt `Array.Length` property as this might cause you issues down the line.

You can also implement a `RunTests()` method to test your implementation. You can specify a limit on the number of swaps your algorithm makes to be sure your algorithm is doing what it's supposed to.

See `BubbleSort.cs` for an example of all of the above.