using System;
using System.Collections.Generic;

namespace CodingExercises
{
    public abstract class SortingAlgorithm
    {
        private int swapCount;

        public static bool IsSorted<T>(T[] A) where T : IComparable {
            for (var i = 0; i < A.Length - 1; i++) {
                if (A[i].Gt(A[i+1])) {
                    return false;
                }
            }

            return true;
        }

        public virtual void RunTests() {
            throw new NotImplementedException();
        }

        public void Sort<T>(T[] A) where T : IComparable {
            this.Sort(A, A.Length);
        }

        public void Sort<T>(T[] A, int length) where T : IComparable {
            this.swapCount = 0;
            SortImpl(A, length);
        }

        public void TestSort<T>(string testName, T[] A, int swapsUpperBound = -1) where T : IComparable {
            Console.Write($"{testName}: ");
            this.Sort(A);
            if (!SortingAlgorithm.IsSorted(A)) {
                Console.WriteLine();
                Console.Write("    FAIL: Array was not sorted");
            } else if (swapsUpperBound >= 0 && this.swapCount > swapsUpperBound) {
                Console.WriteLine();
                Console.Write($"    FAIL: Performed {this.swapCount} but expected no more than {swapsUpperBound} swaps.");
            } else {
                Console.Write($"PASS ({this.swapCount} swaps)");
            }
            Console.WriteLine();
        }

        protected abstract void SortImpl<T>(T[] A, int length) where T : IComparable;

        /// <summary>
        ///  Sort `length` elements of A[] starting from `firstIndex`
        /// </summary>
        protected void SortImplRange<T>(T[] A, int firstIndex, int length) where T : IComparable {
            if (firstIndex >= A.Length) {
                throw new IndexOutOfRangeException($"FirstIndex ({firstIndex}) is out of bounds");
            }
            if (firstIndex + length > A.Length) {
                throw new IndexOutOfRangeException($"Attempted to copy elements beyond length of array");
            }

            var B = new T[length];
            Array.Copy(A, firstIndex, B, 0, length);
            this.SortImpl(B, length);
            Array.Copy(B, 0, A, firstIndex, length);
        }

        protected void Swap<T>(T[] A, int index1, int index2) {
            var tmp = A[index1];
            A[index1] = A[index2];
            A[index2] = tmp;

            this.swapCount++;
        }
    }
}