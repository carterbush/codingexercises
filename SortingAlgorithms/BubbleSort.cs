using System;
using System.Collections.Generic;

namespace CodingExercises
{
    public class BubbleSort : SortingAlgorithm
    {
        protected override void SortImpl<T>(T[] A, int length)
        {
            var didSwaps = true;
            while (didSwaps) {
                didSwaps = false;

                for (var i = 0; i < length - 1; i++) {
                    // Note: You'll need to use Lt(), Leq(), Eq(), Geq(), and Gt()
                    // for less than, less than or equal to, etc.
                    if (A[i].Gt(A[i+1])) {
                        this.Swap(A, i, i + 1);
                        didSwaps = true;
                    }
                }
            }
        }

        public override void RunTests() {
            // Note when copying these tests, the third argument (where supplied) will need to change
            this.TestSort("Basic 1      ", new[] { 5, 9, 2, 7, 4, 3, 6, 1, 8 });
            this.TestSort("Basic 2      ", new[] { 5, 9, 2, 7, 10, 4, 3, 6, 1, 8 });
            this.TestSort("All the same ", new[] { 1, 1, 1, 1, 1, 1, 1, 1, 1 }, 0);
            this.TestSort("In order     ", new[] { 1, 2, 3, 4, 5, 6, 7, 8, 9 }, 0);
            this.TestSort("Reverse order", new[] { 9, 8, 7, 6, 5, 4, 3, 2, 1 }, 36);
        }
    }
}